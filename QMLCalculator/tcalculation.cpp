#include "tcalculation.h"

TCalculation::TCalculation(TLibraryEntryPoint* aPlugin) :
    QObject(NULL),FPlugin(aPlugin)
{
    FTime = 10;
    moveToThread(&FThread);
    connect(&FThread,SIGNAL(started()),SLOT(CalculationItemQueue()));
}

void TCalculation::Add(unsigned int aType, double aOperandA, double aOperandB)
{

    FQueueRequests.Add(qMakePair(aType,qMakePair(aOperandA,aOperandB)));
#if not defined (QT_NO_DEBUG)
    QString iComand = "Starting";
    qDebug(iComand.toLocal8Bit().data());
#endif
    if((FThread.isRunning()==false)||(FThread.isFinished()))
        FThread.start();
}

void TCalculation::CalculationItemQueue()
{
    while(0<FQueueRequests.Count())
    {
        int iTime = 0;
        FMutex.lock();
        iTime = FTime;
        FMutex.unlock();
        FThread.sleep(iTime);

        TItemQueueCalculation iComand = FQueueRequests.Extract();
#if not defined (QT_NO_DEBUG)
        QString idebug = "Try Calculation";
        qDebug(idebug.toLocal8Bit().data());
#endif
        QString iResult = FPlugin->Calculation((TLibraryEntryPoint::TType)iComand.first,iComand.second.first,iComand.second.second);
        emit Calculated(iResult);
#if not defined (QT_NO_DEBUG)
        idebug = "Calculeted="+QString::number(iComand.second.first)+","+QString::number(iComand.second.first)+"->"+iResult;
        qDebug(idebug.toLocal8Bit().data());
#endif
    }

    FThread.exit(0);
}

void TCalculation::Speed(int aTime)
{
    FMutex.lock();
    FTime = aTime;
    FMutex.unlock();

}

unsigned int TCalculation::Count()
{
    return FQueueRequests.Count();
}
