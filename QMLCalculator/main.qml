import QtQuick 2.6
import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.1
Rectangle {
    visible: true
    width: 449
    height: 480
    border.color: "skyblue"

    Item {
        id: neQueue
        property string comand: ""
        property string value: ""
    }
    Item {
        id: operandA
        property string value: ""
    }
    Item {
        id: operandB
        property string value: ""
    }

    Item {
        id: iOperand
        property Item value: operandA
    }

    Item {
        id: tWork
        property int value: -1
        property string label: ""
    }

    signal onDigitCliced(Button sender)
    onOnDigitCliced:{


        iOperand.value.value = iOperand.value.value+sender.text;
        //lDisplay.text = operandA.value+tWork.label+operandB.value;
        lDisplay.text = iOperand.value.value;
    }

    signal onDSCliked()
    onOnDSCliked:{
        mQueue.remove(mQueue.count-1);
    }

    signal onChangeMWCliced(Button sender)
    onOnChangeMWCliced: {


        if(tWork.value==-1)
        {



            if(iOperand.value==operandA)
            {
                lDisplay.text = ""
                switch(sender)
                {
                case button0:tWork.value = 2
                    tWork.label = "/"
                    break
                case button1:tWork.value = 3
                    tWork.label = "*"
                    break
                case button2:tWork.value = 1
                    tWork.label = "-"
                    break
                case button3:tWork.value = 0
                    tWork.label = "+"
                default:
                }
                lDisplay.text = tWork.label
                iOperand.value = operandB;
            }
        }
    }

    Component {
             id: lmHeader
             Row {
                 spacing: 10
                 Text { text: comand }
                 Text { text: '=' + value }
             }
         }

    ListModel
    {
        id: mQueue
    }

    ListView {
        id: l
        width: 418; height: 111
        x:16
        y:70

        model: mQueue
        delegate: lmHeader
    }


    Button {
        id: b1
        x: 14
        y: 374
        text: qsTr("1")
        onClicked: onDigitCliced(this)
    }

    Button {
        id: b4
        x: 14
        y: 328
        text: qsTr("4")
        onClicked: onDigitCliced(this)
    }

    Button {
        id: b7
        x: 14
        y: 282
        text: qsTr("7")
        onClicked: onDigitCliced(this)
    }

    Button {
        id: b8
        x: 120
        y: 282
        text: qsTr("8")
        onClicked: onDigitCliced(this)
    }

    Button {
        id: b5
        x: 120
        y: 328
        text: qsTr("5")
        onClicked: onDigitCliced(this)
    }

    Button {
        id: b2
        x: 120
        y: 374
        text: qsTr("2")
        onClicked: onDigitCliced(this)
    }

    Button {
        id: b0
        x: 226
        y: 420
        text: qsTr("0")
        onClicked: onDigitCliced(this)
    }

    Button {
        id: b9
        x: 226
        y: 282
        text: qsTr("9")
        onClicked: onDigitCliced(this)
    }

    Button {
        id: b6
        x: 226
        y: 328
        text: qsTr("6")
        onClicked: onDigitCliced(this)
    }

    Button {
        id: b3
        x: 226
        y: 374
        text: qsTr("3")
        onClicked: onDigitCliced(this)
    }

    Text {
        id: element
        x: 14
        y: 233
        width: 100
        height: 43
        text: qsTr("Скорость")
        fontSizeMode: Text.Fit
        textFormat: Text.RichText
        lineHeight: 1
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 20
    }

    TextEdit {
        id: eSpead
        x: 120
        y: 236
        width: 100
        height: 43
        text: qsTr("10")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: 20
    }

    Button {
        id: button0
        x: 332
        y: 236
        text: qsTr("/")
        onClicked: onChangeMWCliced(this)
    }

    Button {
        id: button1
        x: 332
        y: 282
        text: qsTr("*")
        onClicked: onChangeMWCliced(this)
    }

    Button {
        id: button2
        x: 332
        y: 328
        text: qsTr("-")
        onClicked: onChangeMWCliced(this)
    }

    Button {
        id: button3
        x: 332
        y: 374
        text: qsTr("+")
        onClicked: onChangeMWCliced(this)
    }

    Button {
        id: button4
        x: 332
        y: 420
        text: qsTr("=")
        onClicked: {
            neQueue.comand = operandA.value+tWork.label+operandB.value+"";
            neQueue.value = "Вычисление";
            mQueue.append(neQueue);
            CPPInterface.addCalculationTask(tWork.value,operandA.value,operandB.value)


            tWork.value = -1
            iOperand.value = operandA
            lDisplay.text = ""
            operandA.value = "";
            operandB.value = "";
            lCountRequests.text = CPPInterface.countRequestCalculation()



        }
    }

    Text {
        id: lDisplay
        x: 14
        y: 187
        width: 418
        height: 40
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: 20
    }


    Timer{
        interval: 1
        repeat: true
        running: true
        onTriggered: {


            while(CPPInterface.isEmptyResult()==false)
            {
                mQueue.get(mQueue.count-CPPInterface.countRequestCalculation()-1).value = CPPInterface.result()
            }
            lCountRequests.text = CPPInterface.countRequestCalculation()
        }
    }

    Button {
        id: button
        x: 226
        y: 236
        text: qsTr("SET")
        onClicked: {
            CPPInterface.speed(eSpead.text)
        }
    }

    Text {
        id: element1
        x: 23
        y: 15
        width: 138
        height: 29
        text: qsTr("Количество запросов")
        font.pixelSize: 12
    }

    Text {
        id: lCountRequests
        x: 175
        y: 15
        width: 34
        height: 14
        text: qsTr("0")
        //text: CPPInterface.countRequestCalculation;
        font.pixelSize: 12
    }

    Button {
        id: button5
        x: 16
        y: 420
        width: 204
        height: 40
        text: qsTr("Очиска экранна примеров")
        onClicked: mQueue.clear()
    }


}
