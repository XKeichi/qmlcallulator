#ifndef TCALCULATION_H
#define TCALCULATION_H

#include <qthread.h>
#include <QPair>
#include "tqueue.h"
#include "Plugins/tlibraryentrypoint.h"
#include <QMutex>
class TCalculation : public QObject
{
    Q_OBJECT
public:
    explicit TCalculation(TLibraryEntryPoint* aPlugin);
    void Add(unsigned int aType, double aOperandA, double aOperandB);
    void Speed(int aTime);
    unsigned int Count();
signals:
    void Calculated(QString aValue);
public slots:
private:
    typedef QPair<double,double>TOperands;
    typedef QPair<unsigned int,TOperands> TItemQueueCalculation;
    TLibraryEntryPoint *FPlugin;
private:
    QThread FThread;
    TQueue<TItemQueueCalculation>FQueueRequests;
    int FTime;
    QMutex FMutex;
private slots:
    void CalculationItemQueue();

};

#endif // TCALCULATION_H
