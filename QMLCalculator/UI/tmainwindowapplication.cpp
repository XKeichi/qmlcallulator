#include "tmainwindowapplication.h"

TMainWindowApplication::TMainWindowApplication(TLibraryEntryPoint* aPlugin)
{
    FDeviceCalculation = new TCalculation(aPlugin);
    connect(FDeviceCalculation,SIGNAL(Calculated(QString)),SLOT(Calculated(QString)));
}
TMainWindowApplication::~TMainWindowApplication()
{
    delete FDeviceCalculation;
}
void TMainWindowApplication::Calculated(QString aValue)
{
#if not defined (QT_NO_DEBUG)
    QString iComand = "Add result queue="+aValue;
    qDebug(iComand.toLocal8Bit().data());
#endif
    FQueueResultCalculation.Add(aValue);
}

bool TMainWindowApplication::addCalculationTask(int aMode, QString aOperandA, QString aOperandB)
{
    bool Ok[2];
#if not defined (QT_NO_DEBUG)
    QString iComand = "Try Add Queue="+QString::number(aMode)+" : "+aOperandA+" , "+aOperandB;
    qDebug(iComand.toLocal8Bit().data());
#endif
    double Operands[2]={aOperandA.toDouble(&Ok[0]),aOperandB.toDouble(&Ok[1])};
    if(Ok[0]&&Ok[1])
    {
#if not defined (QT_NO_DEBUG)
        iComand = "Ok";
        qDebug(iComand.toLocal8Bit().data());
#endif
        setCountRequestCalculation(QString::number(FDeviceCalculation->Count()+1));
        FDeviceCalculation->Add(aMode,Operands[0],Operands[1]);

        return true;
    }
    return false;
}

bool TMainWindowApplication::isEmptyResult()
{
    return (FQueueResultCalculation.Count()==0);
}

const QString TMainWindowApplication::result()
{
    QString iValue = FQueueResultCalculation.Extract();
#if not defined (QT_NO_DEBUG)
    QString iComand = "Extract result queue="+iValue;
    qDebug(iComand.toLocal8Bit().data());
#endif
    return iValue;
}

int TMainWindowApplication::count()
{
    return FQueueResultCalculation.Count();
}

void TMainWindowApplication::speed(int aSeconds)
{
    FDeviceCalculation->Speed(aSeconds);
}

unsigned int TMainWindowApplication::countRequestCalculation()
{
    return FDeviceCalculation->Count();
    //return QString::number(FDeviceCalculation->Count());
}

void TMainWindowApplication::setCountRequestCalculation(QString aValue)
{
    emit countRequestCalculationChanged();
}
