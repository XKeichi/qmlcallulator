#ifndef TMAINWINDOWAPPLICATION_H
#define TMAINWINDOWAPPLICATION_H

#include <QObject>
#include "tqueue.h"
#include "tcalculation.h"
class TMainWindowApplication : public QObject
{
    Q_OBJECT
   // Q_PROPERTY(QString countRequestCalculation READ countRequestCalculation WRITE setCountRequestCalculation NOTIFY countRequestCalculationChanged)
public:
    TMainWindowApplication(TLibraryEntryPoint *aPlugin);
    virtual~TMainWindowApplication();
    Q_INVOKABLE bool addCalculationTask(int aMode, QString aOperandA, QString aOperandB);
    Q_INVOKABLE const QString result();
    Q_INVOKABLE bool isEmptyResult();
    Q_INVOKABLE int count();
    Q_INVOKABLE void speed(int aSeconds);
    Q_INVOKABLE unsigned int countRequestCalculation();
signals:
    void countRequestCalculationChanged();
private:
    TQueue<QString>FQueueResultCalculation;
    TCalculation* FDeviceCalculation;
private:
    void setCountRequestCalculation(QString aValue);
private slots:
    void Calculated(QString aValue);


};

#endif // TMAINWINDOWAPPLICATION_H
