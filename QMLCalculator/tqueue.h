#ifndef TQUEUE_H
#define TQUEUE_H

#include <QVector>
#include <QMutex>

template<class TType>class TQueue
{
public:
    ~TQueue();
    TType Extract();
    void Add(TType aValue);
    unsigned int Count();
private:
    QVector<TType>FQueue;
    QMutex FMutex;
};

template<class TType>TQueue<TType>::~TQueue()
{
    FQueue.clear();
}

template <class TType> TType TQueue<TType>::Extract()
{
    FMutex.lock();
    if(FQueue.isEmpty())
    {
       FMutex.unlock();
       throw QString("Очередь пуста");
    }
    TType rValue = FQueue.first();
    FQueue.erase(FQueue.begin());
    FMutex.unlock();
    return rValue;
}

template <class TType> void TQueue<TType>::Add(TType aValue)
{
    FMutex.lock();
    FQueue.push_back(aValue);
    FMutex.unlock();
}

template <class TType> unsigned int TQueue<TType>::Count()
{
    FMutex.lock();
    unsigned int rCount = (unsigned)FQueue.size();
    FMutex.unlock();
    return rCount;
}

#endif // TQUEUE_H
