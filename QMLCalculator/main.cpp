#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQmlContext>
#include "UI/tmainwindowapplication.h"
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>
#include <QApplication>

TMainWindowApplication* Start(int argc, char *argv[])
{
    QApplication wdapp(argc, argv);
    const QString FileNameConfigure = wdapp.applicationDirPath()+"/Calculator.Configure";
    QSettings iSetup(FileNameConfigure,QSettings::IniFormat);
    const QString LibraryFileName = iSetup.value("Configure/Library","").toString();
    TLibraryEntryPoint *iPlugin=NULL;
#if not defined (QT_NO_DEBUG)
    QString idebug = "Configure file name = "+FileNameConfigure;
    qDebug(idebug.toLocal8Bit().data());
    idebug = "Plugin file name = "+LibraryFileName;
    qDebug(idebug.toLocal8Bit().data());
#endif
    if((QFile::exists(FileNameConfigure))==false||(QFile::exists(LibraryFileName)==false))
    {
        QMessageBox UIMessage;
        UIMessage.setWindowTitle("Калькулятор");
        UIMessage.setText("Отсутствует библиотека обработки данных");
        UIMessage.exec();
        const QString SelectFileLibrary = QFileDialog::getOpenFileName(nullptr,"Выбор файла библиотеки обработки данных");
        if(QFile::exists(SelectFileLibrary))
        {
            if((iPlugin=TLibraryEntryPoint::LoadLibrary(SelectFileLibrary))!=NULL)
            {
                iSetup.setValue("Configure/Library",SelectFileLibrary);
                return new TMainWindowApplication(iPlugin);
            }
            else
            {
                QMessageBox UIError;
                UIError.setWindowTitle("Калькулятор");
                UIError.setText("Ошибка доступа к файлу библиотеки обработки данных");
                UIError.exec();
                return NULL;
            }
        }


    }
    else
    {
        if((iPlugin=TLibraryEntryPoint::LoadLibrary(LibraryFileName))==NULL)
        {

            QMessageBox UIError;
            UIError.setWindowTitle("Калькулятор");
            UIError.setText("Ошибка доступа к файлу библиотеки обработки данных");
            UIError.exec();
            return NULL;
        }
    }
    return new TMainWindowApplication(iPlugin);
}
int main(int argc, char *argv[])
{


    TMainWindowApplication *CPPInterface = Start(argc, argv);

    if(CPPInterface)
    {
        QGuiApplication app(argc, argv);
        QQuickView view;

        view.engine()->rootContext()->setContextProperty("CPPInterface", CPPInterface);
        view.setSource(QUrl(QStringLiteral("qrc:/main.qml")));
        view.setTitle("Калькулятор");

        view.show();

        return app.exec();
    }
    return -1;
}
