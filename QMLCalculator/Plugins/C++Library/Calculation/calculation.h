#ifndef CALCULATION_H
#define CALCULATION_H

//#include "calculation_global.h"
/*
class CALCULATIONSHARED_EXPORT Calculation
{

public:
    Calculation();
};
*/

#ifdef Q_OS_WIN
extern "C"  __declspec(dllexport)double DoIt (int TypeWork, double OperandA, double OperandB, int& ErrorCode);
#else
extern "C" double DoIt (int TypeWork, double OperandA, double OperandB, int& ErrorCode);
#endif


#endif // CALCULATION_H
