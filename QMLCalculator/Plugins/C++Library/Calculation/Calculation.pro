#-------------------------------------------------
#
# Project created by QtCreator 2020-06-28T03:20:52
#
#-------------------------------------------------

QT       -= core gui

TARGET = Calculation
TEMPLATE = lib

DEFINES += CALCULATION_LIBRARY

SOURCES += calculation.cpp

HEADERS += calculation.h\
        calculation_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
