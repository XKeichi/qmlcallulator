#include "calculation.h"

/*
Calculation::Calculation()
{
}
*/
double DoIt (int TypeWork, double OperandA, double OperandB, int& ErrorCode)
{
    double rValue=0;
    ErrorCode=0;
    switch(TypeWork)
    {
    case 0:
        rValue=OperandA+OperandB;
        break;
    case 1:
        rValue=OperandA-OperandB;
        break;
    case 2:
        if(OperandB==0)
            ErrorCode=-1;
        else
            rValue=OperandA/OperandB;
        break;
    case 3:
        rValue = OperandA*OperandB;
    }
    return rValue;
}
