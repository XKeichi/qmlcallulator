#include "tlibraryentrypoint.h"
#include <math.h>
#if not defined (QT_NO_DEBUG)
#include <QtDebug>
#endif
TLibraryEntryPoint::TLibraryEntryPoint(QObject *parent) :
    QObject(parent)
{
    FDoIt = NULL;
    FLibrary = NULL;
}

TLibraryEntryPoint::~TLibraryEntryPoint()
{
    if(FLibrary)
    {
        FLibrary->unload();
        delete FLibrary;
        FLibrary = NULL;
    }
}

TLibraryEntryPoint* TLibraryEntryPoint::LoadLibraryW(QString aFileName)
{
#if not defined (QT_NO_DEBUG)
    QString idebug = "Load Plugin library: "+aFileName;
    qDebug(idebug.toLocal8Bit().data());
#endif
    TLibraryEntryPoint *rValue = NULL;
    QLibrary* Library = new QLibrary;

    Library->setFileName(aFileName);

    if(Library->load())
    {
        TDoIt iDoit;
        if((iDoit=(TDoIt)Library->resolve("DoIt"))!=NULL)
        {
            rValue =  new TLibraryEntryPoint;
            rValue->FLibrary = Library;
            rValue->FDoIt = iDoit;
            int ErrorCode;
            QString Testing = "---->Testing: ";//+QString::number(iDoit(0,1,2,ErrorCode));
             qDebug(Testing.toLocal8Bit().data());
            return rValue;
        }
        Library->unload();
    }
    delete Library;
    Library = NULL;
    return NULL;
}

QString TLibraryEntryPoint::Calculation(TLibraryEntryPoint::TType acType, double OperandA, double OperandB)
{
    if(FDoIt==NULL)
    {
        return "Ощибка указатель функции равен нулю";
    }
    int ErrorCode=0;
    double iValue = FDoIt(acType,OperandA,OperandB,ErrorCode);
#if not defined (QT_NO_DEBUG)
    QString idebug = "Ok";
    qDebug(idebug.toLocal8Bit().data());
#endif
    if(ErrorCode==-1)
        return "Ошибка";
    const unsigned int Count = -log10(iValue-(int)iValue);
    QString rValue = QString::number(iValue,'f',Count);
#if not defined (QT_NO_DEBUG)
    QString iComand = QString::number(acType)+" : "+QString::number(OperandA)+" , "+QString::number(OperandB)+"->"+rValue;
    qDebug(iComand.toLocal8Bit().data());
#endif
    return rValue;
}

