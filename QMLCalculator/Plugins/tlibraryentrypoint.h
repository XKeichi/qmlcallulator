#ifndef TLIBRARYENTRYPOINT_H
#define TLIBRARYENTRYPOINT_H

#include <QLibrary>

class TLibraryEntryPoint : public QObject
{
    Q_OBJECT
public:
    enum TType {ctAdding, ctSubtraction, ctDivision, ctMultiplication};
public:
    virtual ~TLibraryEntryPoint();
    virtual QString Calculation(TType acType, double OperandA, double OperandB);
    static TLibraryEntryPoint *LoadLibraryW(QString aFileName);
private:
    typedef double (*TDoIt)(int TypeWork, double OperandA, double OperandB, int& ErrorCode);
private:
    TDoIt FDoIt;
    QLibrary *FLibrary;
private:
    explicit TLibraryEntryPoint(QObject *parent = 0);

signals:
    void Result();
public slots:

};

#endif // TLIBRARYENTRYPOINT_H
